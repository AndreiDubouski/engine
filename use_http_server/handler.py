# !/usr/bin/python
import json
from http.server import BaseHTTPRequestHandler, HTTPServer


answer = {'granted': True, 'validity': 5}
msg_allowed = json.dumps(answer)
answer['granted'] = False
msg_forbidden = json.dumps(answer)

HOST_NAME = "localhost"
PORT_NUMBER = 8000

# This class will handles any incoming request
class myHandler(BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.1'

    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header("Connection", "keep-alive")
        self.end_headers()

    # Handler for the POST requests
    def do_POST(self):
        print("incomming: ", str(self.path))
        print(str(self.headers.keys()))
        print(str(self.headers.values()))

        self._set_response()
        if self.headers['my-auth-header'] == 'good-token':
            self.wfile.write(bytes(msg_allowed, "utf-8"))
            print(msg_allowed)
        else:
            self.wfile.write(bytes(msg_forbidden, "utf-8"))
            print(msg_forbidden)

        return

try:
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print ('Started httpserver on port ', PORT_NUMBER)

    # Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    print ('^C received, shutting down the web server')
    server.socket.close()
