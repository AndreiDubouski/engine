Orthanc + Advanced Authorization Plugin 

    sudo apt-get update 
    sudo apt-get install orthanc 
    sudo apt-get install nginx

"libOrthancAuthorization.so" плагин необходим положить:

    /usr/share/orthanc/plugins/libOrthancAuthorization.so

"orthanc.json" конфигурацию для Orthanc положить:

    /etc/orthanc/orthanc.json

перезагрузка Orthanc

    sudo /etc/init.d/orthanc restart

запуск прокси сервера c указанием конфигурации "my_config"

    sudo nginx -c /home/andrei/Documents/my_config -s stop
    sudo nginx -c /home/andrei/Documents/my_config

запуск сервиса аутентификации

    python3 handler.py 


в браузере 

    http://localhost/allowed/app/explorer.html
    http://localhost/forbidden/app/explorer.html
завершить Ctrl+C


остановка прокси сервера nginx

    sudo nginx -c /home/andrei/Documents/my_config -s stop 

перезагрузка Orthanc

    sudo /etc/init.d/orthanc stop