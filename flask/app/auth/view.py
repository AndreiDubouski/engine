from app import authentication_app
from flask import render_template, request, make_response, session, flash

import logging
from flask import jsonify


# app.logger.warning('testing warning log')
# app.logger.error('testing error log')
# app.logger.info('testing info log')


# Catch all routes for Flask
@authentication_app.route('/', defaults={'path': ''})
@authentication_app.route('/<path:path>')
def catch_all(path):
    d = request
    authentication_app.logger.info(d.authorization)
    authentication_app.logger.info(d.base_url)
    authentication_app.logger.info(d.method)
    authentication_app.logger.info(request.headers)

    data = request.data.decode("utf-8")
    authentication_app.logger.info(data)

    if request.method == 'POST':

        authentication_app.logger.info(' POST testing info log')
        authentication_app.logger.info(d.data)

        answer = {
            'granted': 'True',
            'validity': 5
        }
        resp = make_response(jsonify(answer), 200)
        resp.mimetype = 'application/json'
        # resp.headers['X-Something'] = 'A value'
        authentication_app.logger.info(resp)
        return resp
        # return 'POST'
    else:
        authentication_app.logger.info(' other testing info log')
        authentication_app.logger.info(d.data)
        answer = {
            'granted': 'False',
            'validity': 5
        }
        resp = make_response(jsonify(answer), 401)
        resp.mimetype = 'application/json'
        # resp.headers['X-Something'] = 'A value'
        authentication_app.logger.info(resp)
        return resp
        # return 'other'



@authentication_app.route('/test')
def home():
    return 'test'






@authentication_app.route('/')
def process_all(path):
    d = request
    authentication_app.logger.info(d.authorization)
    authentication_app.logger.info(d.base_url)
    authentication_app.logger.info(d.method)
    authentication_app.logger.info(request.headers)

    data = request.data.decode("utf-8")
    authentication_app.logger.info(data)

    if request.method == 'POST':

        authentication_app.logger.info(' POST testing info log')
        authentication_app.logger.info(d.data)

        answer = {
            'granted': 'true',
            'validity': 5
        }
        resp = make_response(jsonify(answer), 200)
        resp.mimetype = 'application/json'
        # resp.headers['X-Something'] = 'A value'
        authentication_app.logger.info(resp)
        return resp
        # return 'POST'
    else:
        authentication_app.logger.info(' other testing info log')
        authentication_app.logger.info(d.data)
        answer = {
            'granted': 'false',
            'validity': 5
        }
        resp = make_response(jsonify(answer), 401)
        resp.mimetype = 'application/json'
        # resp.headers['X-Something'] = 'A value'
        authentication_app.logger.info(resp)
        return resp
        # return 'other'