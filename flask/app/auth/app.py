from flask import Flask
from config import Configuration

authentication_app = Flask(__name__)
# set config
authentication_app.config.from_object(Configuration)
