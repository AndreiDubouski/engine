from config import Configuration
from flask import Flask

app = Flask(__name__)
# set config
app.config.from_object(Configuration)
