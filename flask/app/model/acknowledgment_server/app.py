from config import Configuration
from flask import Flask

acknowledgment_app = Flask(__name__)
# set config
acknowledgment_app.config.from_object(Configuration)
