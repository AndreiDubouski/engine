from app import acknowledgment_app
from flask import render_template, request, session

import json

answer = {'granted': True, 'validity': 5}
msg_allowed = json.dumps(answer)
answer['granted'] = False
msg_forbidden = json.dumps(answer)

import logging


# app.logger.warning('testing warning log')
# app.logger.error('testing error log')
# app.logger.info('testing info log')

@acknowledgment_app.after_request
def do_something_whenever_a_request_has_been_handled(response):
    acknowledgment_app.logger.info('after_request')
    # we have a response to manipulate, always return one
    acknowledgment_app.logger.info('method: %s' % request.method)
    acknowledgment_app.logger.info('headers: %s' % request.headers)
    acknowledgment_app.logger.info(request.headers['my-auth-header'])
    acknowledgment_app.logger.info('\r\n')
    # acknowledgment_app.logger.info(request.data)

    data = json.decoder(request.data)
    acknowledgment_app.logger.info(data)
    if request.method == 'POST':

        if request.headers['my-auth-header'] == 'good-token':
            response = acknowledgment_app.response_class(response=msg_allowed,
                                                         status=200,
                                                         mimetype='application/json',
                                                         content_type='application/json')
            return response
        elif request.headers['my-auth-header'] == 'bad-token':
            response = acknowledgment_app.response_class(response=msg_forbidden,
                                                         status=200,
                                                         mimetype='application/json',
                                                         content_type='application/json')
            return response
    else:
        response = acknowledgment_app.response_class(response=msg_forbidden,
                                                     status=405)

    return response


'''@acknowledgment_app.route('/', defaults={'path': ''})
@acknowledgment_app.route('/<path:path>')
def catch_all(path):
    acknowledgment_app.logger.info('url ath: %s' % path)
    acknowledgment_app.logger.info('method: %s' % request.method)
    acknowledgment_app.logger.info('headers: %s' % request.headers)

    if request.method == 'POST':
        response = acknowledgment_app.response_class(response=msg_allowed,
                                                     status=200,
                                                     mimetype='application/json',
                                                     content_type='application/json')
        return response
    else:
        response = acknowledgment_app.response_class(response=msg_forbidden,
                                                     status=405)
        return response

    return 'You want path: %s' % path
'''