from app import acknowledgment_app
import view

if __name__ == '__main__':
    acknowledgment_app.run(host='0.0.0.0', port=8000)
