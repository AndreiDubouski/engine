import os


class Configuration(object):
    DEBUG = True
    SECRET_KEY = os.urandom(12)
    PORT = 8000
