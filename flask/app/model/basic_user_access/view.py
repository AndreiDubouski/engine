from app import basic_user_access_app
from flask import render_template, request, redirect, url_for, session, flash

from flask_login import current_user, login_user
from models import User
from forms import LoginForm

from functools import wraps
from flask import request, Response

import json

answer = {'granted': True, 'validity': 5}
msg_allowed = json.dumps(answer)


# app.logger.warning('testing warning log')
# app.logger.error('testing error log')
# app.logger.info('testing info log')


@basic_user_access_app.route('/test')
def test():
    basic_user_access_app.logger.info('method: %s' % request.method)
    basic_user_access_app.logger.info('headers: %s' % request.headers)

    if request.method == 'POST':
        response = basic_user_access_app.response_class(response=msg_allowed,
                                                        status=200,
                                                        mimetype='application/json',
                                                        content_type='application/json')
        return response

    return '{"test":"test"}'


@basic_user_access_app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {} password {}'.format(form.username.data, form.password))

        basic_user_access_app.logger.info('method: %s' % request.method)
        basic_user_access_app.logger.info(User.get_allowed_users())

        return redirect('/index')

    return render_template('login.html', title='Sign In', form=form)


@basic_user_access_app.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('index.html', title='Home')


@basic_user_access_app.route("/logout")
def logout():
    session['logged_in'] = False
    return test()


# secure App
def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    possible_users = User.get_allowed_users()
    return username == 'admin' and password == 'secret'


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)

    return decorated


@basic_user_access_app.route('/secret-page')
@requires_auth
def secret_page():
    return 'secret-page'
