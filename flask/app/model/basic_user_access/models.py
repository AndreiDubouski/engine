from flask_login import UserMixin
from app import login
import json


class User(UserMixin):

    def __init__(self, name, password, role):
        self.name = name
        self.password = password
        self.role = role

    @staticmethod
    def get_allowed_users(path_config='/etc/orthanc/orthanc.json'):
        #  path to config:      '/etc/orthanc/orthanc.json'
        #
        #   "AuthenticationEnabled":true,
        #   "RegisteredUsers":{
        #      "writer":"writer",
        #      "reader":"reader",
        #      "alice":"alice",
        #      "bob":"bob"

        with open(path_config) as f:
            data = json.load(f)

        if data is not None:
            if data['AuthenticationEnabled'] and data['RegisteredUsers']:
                possible_users = list(data['RegisteredUsers'])
                return possible_users

        else:
            return None






@login.user_loader
def load_user(id):
    return User.get(int(id))
