from config import Configuration
from flask import Flask

from flask_login import LoginManager

basic_user_access_app = Flask(__name__)
# set config
basic_user_access_app.config.from_object(Configuration)

login = LoginManager(basic_user_access_app)