import json
import pprint


class User(object):

    def __init__(self, name, password, job):
        self.name = name
        self.password = password
        self.job = job

    def get_allowed_users(self, path_config='/etc/orthanc/orthanc.json'):
        #  path to config:      '/etc/orthanc/orthanc.json'
        #
        #   "AuthenticationEnabled":true,
        #   "RegisteredUsers":{
        #      "writer":"writer",
        #      "reader":"reader",
        #      "alice":"alice",
        #      "bob":"bob"

        with open(path_config) as f:
            data = json.load(f)
            pprint(data)

        if data is not None:
            if data['AuthenticationEnabled'] and data['RegisteredUsers']:
                possible_users = list(data['RegisteredUsers'])
                return possible_users

        else:
            return None
