import json

answer = {'granted': True, 'validity': 5}
msg_allowed = json.dumps(answer)
answer['granted'] = False
msg_forbidden = json.dumps(answer)

# !/usr/bin/python
from http.server import BaseHTTPRequestHandler, HTTPServer

HOST_NAME = "localhost"
PORT_NUMBER = 8000


# This class will handles any incoming request from
# the browser
class myHandler(BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.1'

    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header("Connection", "keep-alive")
        # self.send_header("Transfer-Encoding", "chunked")

        # self.send_header("Accept", "text/plain")
        # self.send_header("Charset", "utf-8")
        self.end_headers()

    # Handler for the POST requests
    def do_POST(self):
        print("incomming: ", str(self.path))
        print(str(self.headers.keys()))
        print(str(self.headers.values()))

        # self.wfile.write(msg)
        self._set_response()
        if self.headers['my-auth-header'] == 'good-token':
            self.wfile.write(bytes(msg_allowed, "utf-8"))
            print(msg_allowed)
        else:
            self.wfile.write(bytes(msg_forbidden, "utf-8"))
            print(msg_forbidden)

        # self.wfile.write(bytes("\r\n", "utf-8"))

        return


try:
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print ('Started httpserver on port ', PORT_NUMBER)

    # Wait forever for incoming htto requests
    server.serve_forever()

except KeyboardInterrupt:
    print ('^C received, shutting down the web server')
    server.socket.close()
