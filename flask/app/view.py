from app import app
from flask import render_template, request, session, flash


import json
import logging
#app.logger.warning('testing warning log')
#app.logger.error('testing error log')
#app.logger.info('testing info log')

class User(object):

    def __init__(self, name, password, job):
        self.name = name
        self.password = password
        self.job = job



def get_allowed_users(path_config='/etc/orthanc/orthanc.json'):
    #  path to config:      '/etc/orthanc/orthanc.json'
    #
    #   "AuthenticationEnabled":true,
    #   "RegisteredUsers":{
    #      "writer":"writer",
    #      "reader":"reader",
    #      "alice":"alice",
    #      "bob":"bob"

    with open(path_config) as f:
        data = json.load(f)

    if data is not None:
        if data['AuthenticationEnabled'] and data['RegisteredUsers']:
            possible_users = list(data['RegisteredUsers'])
            return possible_users

    else:
        return None



@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        name = 'Ivan'
        return render_template('index.html', name=name)


#@app.route('/login', methods=['GET'])
#def do_login():
#    """ GET this is log in """
#    return home()


@app.route('/login', methods=['POST'])
def do_login():
    """POST this is log in"""
    app.logger.info(get_allowed_users())

    if request.form['password'] == 'password' and request.form['username'] == 'admin':
        session['logged_in'] = True

        name = 'Ivan'
        return render_template('index.html', name=name)

    else:
        flash('wrong password!')
        return home()


@app.route("/logout")
def logout():
    """ this is log out"""
    session['logged_in'] = False
    return home()
